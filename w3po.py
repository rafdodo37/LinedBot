#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script assurant la diffusion des notifications de W3PO à ses abonnés

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys, getopt
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import requests

import pywikibot
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
site = pywikibot.Site('fr', 'wikipedia')
nbrModif = 0
nbrTotal = 0

# Retourne la liste des personnes à qui envoyer la notification
def getSubscribers(group, hs):

    pageGroup = pywikibot.Page(site, "Utilisateur:Orikrin1998/Blog/Abonnés/"+group.title()+"s")
    pageTotal = pywikibot.Page(site, "Utilisateur:Orikrin1998/Blog/Abonnés/Wikipédia seulement")

    pageHS = pywikibot.Page(site, "Utilisateur:Orikrin1998/Blog/Abonnés/Tous les articles")

    if not hs:
        subscribers = set(list(pageGroup.linkedPages(namespaces=3)) + list(pageTotal.linkedPages(namespaces=3)) + list(pageHS.linkedPages(namespaces=3)))

    if hs:
        subscribers = set(list(pageHS.linkedPages(namespaces=3)))

    return subscribers

#Retourne le titre associé à l'URL
def getURLTitle(url):
    webPage = requests.get(url)
    text = webPage.text
    tt = text.split("<title>") [1]
    titleFull = tt.split("</title>") [0]
    title = titleFull.split(" | Blog d'un geek polyglotte") [0]

    return title


#Exécution
def main():

    subtype = ''
    link = ''
    hs = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h', ['hs'])
    except getopt.GetoptError:
        sys.exit(2)

    for opt, _ in opts:
        if opt in ('-h', '--hs'):
            hs = True


    if len(args) == 2:
        if args[0] in ('article', 'sondage', 'interview'):
            subtype = args[0]
        else:
            pywikibot.output("type incorrect : attendu 'article', 'sondage' ou 'interview'")
            sys.exit(2)

        if args[1] != '':
            link = args[1]
        else:
            pywikibot.output("lien obligatoire")
            sys.exit(2)

    else:
        pywikibot.output("usage : python " + sys.argv[0] + " [--hs] type_de_billet lien")
        sys.exit(2)

    banner = "{{Utilisateur:Orikrin1998/Blog/Annonce|type=" + subtype + "|lien=" + link + "|date=~~~~~}}"
    billTitle = getURLTitle(link)
    if type == "interview":
        billTitle = "Interview de " + billTitle

    subscribers = getSubscribers(subtype, hs)

    for sub in subscribers:
        try:
            sub.get()

        except pywikibot.exceptions.NoPageError:
            pywikibot.output("Page %s does not exist; skipping."
                             % sub.title(as_link=True))
        except pywikibot.exceptions.IsRedirectPageError:
            pywikibot.output("Page %s is a redirect; skipping."
                             % sub.title(as_link=True))
        except pywikibot.exceptions.LockedPageError:
            pywikibot.output("Page %s is locked; skipping."
                             % sub.title(as_link=True))
        else:
            message = "\n\n== Du nouveau sur W3PO : " + billTitle + " ==\n" + banner
            sub.text += message
            try:
                sub.save("Du nouveau sur W3PO", minor=False)
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.output('%s %s'% (type(myexception), myexception.args))

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

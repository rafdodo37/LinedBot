#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de maintenance pour vikidia


# (C) Linedwell, 2011-2023
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import mylogging # pylint: disable=unused-import

import re

import pywikibot
import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
site = pywikibot.Site('fr', 'vikidia')
nbrModif = 0
nbrTotal = 0

# Traitement des pages de la catégorie
def clean_broken_file_links_cat() -> str:
    log = ''

    bfl_cat = pywikibot.Category(site, "Pages_avec_des_liens_de_fichiers_brisés")
    bfl_pages = list(bfl_cat.articles(namespaces=0))

    for page in bfl_pages:
        print(f"{page.title()=}")
        imagelinks = page.imagelinks()
        c = callback.Callback() #(re)init de c
        for image in imagelinks:
            try:
                _ = image.latest_file_info
            except (pywikibot.exceptions.NoPageError, pywikibot.exceptions.PageRelatedError): #The page doesn't exist
                imagename = image.title(with_ns=False)
                imagename = re.escape(imagename)
                print(f"{imagename=}")
                parser = re.compile(r'(\[\[)?(Fichier:|File:|Image:)?' + imagename + r'(.*\]\])?', re.I | re.U)
                searchResult = parser.search(page.text)
                if searchResult:
                    page.text = parser.sub('', page.text)
        try:
            page.save("[[VD:Robot|Robot]] : Suppression de lien vers image inexistante", callback=c, force=True)
        except pywikibot.exceptions.PageSaveRelatedError as myexception:
            pywikibot.error('%s : %s'% (page.title(), myexception.args))

    return log

# Exécution
def main():
    clean_broken_file_links_cat()


if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()



#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de nettoyage du bac à sable de Vikidia

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys, getopt
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import pywikibot
from datetime import datetime, timedelta

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
dicoES = {
    'site' : pywikibot.Site('es', 'vikidia'),
    'page' : "Vikidia:Zona de juegos",
    'template' : "{{subst:Vikidia:Zona de juegos/0}}",
    'summary' : "[[Vikidia:Bot|Bot]]: Limpiando zona de juegos",
    'delay' : 30,
}

dicoFR = {
    'site' : pywikibot.Site('fr', 'vikidia'),
    'page' : "Vikidia:Bac à sable",
    'template' : "{{subst:Vikidia:Bac à sable/Zéro}}",
    'summary' : "[[Vikidia:Robot|Robot]] : Nettoyage du bac à sable",
    'delay' : 30,
}

dicoIT = {
    'site' : pywikibot.Site('it', 'vikidia'),
    'page' : "Vikidia:Area giochi",
    'template' : "{{subst:Vikidia:Area giochi/Zero}}",
    'summary' : "[[Vikidia:Bot|Bot]] : Pulizia dell'area giochi",
    'delay' : 30,
}

dicoPT = {
    'site' : pywikibot.Site('pt', 'vikidia'),
    'page' : "Vikidia:Zona de testes coletivos",
    'template' : "{{subst:Vikidia:Zona de testes coletivos/Zero}}",
    'summary' : "[[Vikidia:Bot|Bot]] : Fazendo limpeza da zona de testes coletivos",
    'delay' : 30,
}

dicoSCN = {
    'site' : pywikibot.Site('scn', 'vikidia'),
    'page' : "Vikidia:Fogghiu di prova",
    'template' : "{{subst:Vikidia:Fogghiu di prova/Zeru}}",
    'summary' : "[[Vikidia:Bot|Bot]] : Canciamentu n'arreri dô fogghiu di prova",
    'delay' : 30,
}

#Recharge le bac à sable avec un contenu prédéfini
def clean(dico: dict, force=False):
    site = dico['site']
    page = pywikibot.Page(site, dico['page'])
    template = dico['template']
    summary = dico['summary']
    delay = dico['delay']
    site.login() # force login for below check
    if not page.userName() == site.user():
        limite = calcLimit(delay)
        if (page.latest_revision.timestamp < limite) or force:
            page.text = template
            try:
                page.save(summary)
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.error('%s %s'% (type(myexception), myexception.args))

#Calcule la "date" avant laquelle on s'autorise à blanchir le bas
def calcLimit(delay: int) -> 'datetime.datetime':
    today = datetime.utcnow()
    limit = today - timedelta(minutes=delay)
    return limit


#Exécution
def main():
    force = False

    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'f', ['force'])
    except getopt.GetoptError:
        sys.exit(2)

    for opt, _ in opts:
        if opt in ('-f', '--force'):
            force = True

    clean(dicoES, force) #nettoyage es
    clean(dicoFR, force) #nettoyage fr
    clean(dicoIT, force) #nettoyage it
    clean(dicoPT, force) #nettoyage pt
    clean(dicoSCN, force) #nettoyage scn

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

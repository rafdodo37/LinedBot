#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de nettoyage automatique de la page des PàS

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html


import sys, getopt
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import re
import time
from datetime import date
import pywikibot
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

#Variables globales
site = pywikibot.Site('fr', 'wikipedia')

delimiterBegin = "= Propositions =\n"
delimiterEnd = "= Maintenance : mise à jour =\n"
    
delimiterBeginRegex = r"=\s*Propositions\s*=\n"
delimiterEndRegex = r"=\s*Maintenance : mise à jour\s*=\n"

month = ["", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]
summary = ""

#Créé la section PàS du lendemain
def pasNewSection(pageTemp: str) -> str:
    global month, summary
    
    pad = 1 #nombre de jours de decalage que l'on souhaite
    dateD = time.localtime(time.time() + pad * 24 * 3600)
    year = int(dateD.tm_year)
    monthNum = int(dateD.tm_mon)
    dayNum = int(dateD.tm_mday)
    
    if pageTemp.find("== " + str(dayNum) + " " + month[monthNum] + " ==") == -1: #La section de dans 'pad' n'existe pas encore
        pageTempBegin, pageTempEnd = re.split(delimiterEndRegex, pageTemp)
        pageTempEnd = delimiterEnd + pageTempEnd
        
        newSection = "== " + str(dayNum) + " " + month[monthNum] + " ==\n"
        newSection += "{{En-tête section Débat d'admissibilité|" + str(dayNum)+ "|" + month[monthNum] + "|" + str(year) + "}}\n\n"
        newSection += "{{Boîte déroulante/début|titre=Requêtes traitées}}\n{{Boîte déroulante/fin}}\n\n"
        
        pageTemp = pageTempBegin + newSection + pageTempEnd
        
        if summary != '':
            summary += " ; initialisation de la section du %s %s" %(dayNum, month[monthNum])
        else:
            summary += "initialisation de la section du %s %s" %(dayNum, month[monthNum])
    
    else:
        pywikibot.output("Aucune modification, la section du %s %s existe."
                         %(dayNum, month[monthNum]))

    return pageTemp

#Archive les sections vidées
def pasRemoveSection(pageTemp: str) -> str:
    global month, summary

    pageTempBegin, pageTempBody = re.split(delimiterBeginRegex, pageTemp)
    pageTempBegin += delimiterBegin
    
    pageTempBody, pageTempEnd = re.split(delimiterEndRegex, pageTempBody)
    pageTempEnd = delimiterEnd + pageTempEnd
    
    section = re.split(r'(==\s*\d*(?:er)? \w*\s*==\n)', pageTempBody, flags=re.U)
    parser = re.compile(r'{{En-tête section (?:PàS|Débat d\'admissibilité)\|.*?}}\s*{{Boîte déroulante\/début\|.*?}}', re.U | re.I | re.M)

    today = date.today()
    parsertoday = re.compile(r'==\s*%d\s%s\s*==' %(today.day, month[today.month]), re.U | re.I)
    
    pageTempBody = ''
    count = 0

    for i in range(2, len(section), 2):
        result = parser.search(section[i])
        if result:
            isToday = parsertoday.search(section[i-1])
            if not isToday:
                section[i] = section[i-1] = ''
                count += 1
            else:
                pywikibot.output("Aucune modification, pas d'archivage sur la section du jour.")


    if count > 0:
        for s in section:
            pageTempBody += s

        pageTemp = pageTempBegin + pageTempBody + pageTempEnd
        summary = "archivage de %s section(s)" %(count)

    return pageTemp

#Exécution
def main(argv):
    global summary
    
    archiveOnly = False
    try:
        opts, _ = getopt.getopt(argv, 'a', ['archive'])
    except getopt.GetoptError:
        pywikibot.error("usage: pas.py [-a]")
        exit(2)

    for opt, _ in opts:
        if opt in ('-a', '--archive'):
            archiveOnly = True

    summary = ''
    target = "Wikipédia:Débat d'admissibilité"
    page = pywikibot.Page(site, target)

    try:
        pageTemp = page.get()
        
    except pywikibot.exceptions.NoPageError:
        pywikibot.error("Page %s does not exist; skipping."
                         % page.title(as_link=True))
    except pywikibot.exceptions.IsRedirectPageError:
        pywikibot.error("Page %s is a redirect; skipping."
                         % page.title(as_link=True))
    except pywikibot.exceptions.LockedPageError:
        pywikibot.error("Page %s is locked; skipping."
                         % page.title(as_link=True))
    else:

        pageTempNew = pasRemoveSection(pageTemp)

        if not archiveOnly:
            pageTempNew = pasNewSection(pageTempNew)

        if pageTempNew != pageTemp:
            page.text = pageTempNew
            try:
                page.save("[[WP:Bot|Robot]] : " + summary)
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.error('%s %s'% (type(myexception), myexception.args))

        else:
            pywikibot.output("Aucune action aujourd'hui, archivage et section du lendemain non requis.")
    


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    finally:
        pywikibot.stopme()

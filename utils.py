#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ensemble de fonctions utilitaires

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

from datetime import datetime, timedelta

#Retourne la date avant laquelle on considère obsolète l'usage du modèle
def calcLimit(delay: int) -> 'datetime.datetime':
    today = datetime.utcnow()
    limite = today - timedelta(days=delay)
    return limite

#Retourne le temps écoulé entre une date et le jour courant
def calcDuration(date: 'datetime.datetime') -> 'datetime.timedelta':
    today = datetime.utcnow()
    duration = today - date
    return duration

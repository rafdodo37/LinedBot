#!/usr/bin/python3
# -*- coding: utf-8 -*-

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html


import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import time
from datetime import datetime, timedelta

import pywikibot

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

#Variables globales

#site = pywikibot.Site()
month = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]

summaryHeader = {
    "wikipedia" : "[[WP:Bot|Robot]] : ",
    "vikidia" : "[[VD:Robot|Robot]] : ",
}

nbrModif = 0
nbrTotal = 0

#Met à jour la page de journalisation du bot
def editLog(site: 'pywikibot.site', log: str, page="Utilisateur:LinedBot/Log", summary='', ar=True, cl=False):
    if log != '':
        family = site.family.name
        year = time.strftime('%Y')
        pageLog = pywikibot.Page(site, page)
        pageArchive = pywikibot.Page(site, pageLog.title() + '/' + str(int(year) - 1))
        
        if ar and not pageArchive.exists():
            pageLogTemp = archive(site, pageLog, pageArchive)
        else:
            pageLogTemp = pageLog.get()
        
        if cl > 0:
            pageLogTemp = clean(pageLogTemp, cl)

        if pageLogTemp.find("== " + month[int(time.strftime('%m'))] + " ==") == -1: pageLogTemp += "\n\n== " + month[int(time.strftime("%m"))] + " =="
        if pageLogTemp.find("=== " + time.strftime('%Y-%m-%d') + " ===") != -1: pageLogTemp += '\n' + log
        else :
            pageLogTemp += '\n' + "=== " + time.strftime('%Y-%m-%d') + " ===\n" + log
        if summary == '':
            summary = summaryHeader[family] + "Mise à jour du journal (OK:%s, KO:%s)" %(nbrModif, (nbrTotal - nbrModif))
        else:
            summary = summaryHeader[family] + summary
        
        pageLog.get(force=True, get_redirect=True)
        pageLog.text = pageLogTemp
        try:
            pageLog.save(summary)
        except pywikibot.exceptions.PageSaveRelatedError as myexception:
            pywikibot.error('%s %s'% (type(myexception), myexception.args))
        

#Archive la page de journalisation du bot et réinitialise la page pour la nouvelle année
def archive(site: 'pywikibot.site', pageLog: 'pywikibot.page.Page', pageArchive: 'pywikibot.page.Page') -> str:
    family = site.family.name
    pageLog.move(pageArchive.title(), "Archivage annuel") #Déplacement de pageLog vers pageArchive
    pageArchive = pywikibot.Page(site, pageArchive.title())
	
    #Retrait du modèle de mise à jour de pageArchive
    pageArchiveTemp = pageArchive.get(force=True, get_redirect=True)
    pageArchiveTemp = pageArchiveTemp.replace('{{Mise à jour bot|Linedwell}}\n', '', 1)
    summary = summaryHeader[family] + "Retrait de {{Mise à jour bot}}, page d'archive n'étant plus mise à jour"
    pageArchive.text = pageArchiveTemp
    try:
        pageArchive.save(summary, force=True)
    except pywikibot.exceptions.PageSaveRelatedError as myexception:
        pywikibot.error('%s %s'% (type(myexception), myexception.args))
	
    pageLogTemp = "__NOINDEX__\n{{Mise à jour bot|Linedwell}}\n{{Sommaire|niveau=1}}\n" #On réinsère le modèle de màj sur pageLog
    return pageLogTemp

#Supprime les sections plus vieilles que X jours
def clean(pageTemp: str, days=30) -> str:
    limit = datetime.utcnow() - timedelta(days=days)
    date = limit.strftime('%Y-%m-%d')
    index = pageTemp.find("=== " + date + " ===")
    if index != -1:
        pageHeader = "__NOINDEX__\n{{Mise à jour bot|Linedwell}}\n{{Palette|Admissibilité à vérifier}}\n{{Sommaire|niveau=2}}\n{{/graphe}}\n\n"
        monthSection = "== %s ==\n" %(month[int(limit.strftime('%m'))])
        
        pageTemp = pageHeader + monthSection + pageTemp[index:]
    return pageTemp

def setValues(nbTotal: int, nbModif: int):
    global nbrTotal, nbrModif
    nbrTotal = nbTotal
    nbrModif = nbModif

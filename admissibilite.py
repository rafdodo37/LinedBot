#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script indiquant les changements récents (ajouts / retraits) dans la Catégorie:Admissibilité à vérifier

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html


import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import time
import shutil

import mylogging # pylint: disable=unused-import

import pywikibot
from pywikibot import pagegenerators
import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import
import grapher

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
site = pywikibot.Site('fr', 'wikipedia')

def admissibilite(pagesList: Iterator['pywikibot.page.Page']) -> Tuple[str, str, int]:
    log = ''
    nbAdd = 0
    nbRem = 0
    backupList = loadBackupFile()
    actualList = titleList(pagesList)
    total = len(actualList)

    addList = list(set(actualList) - set(backupList))
    remList = list(set(backupList) - set(actualList))

    for add in addList:
        log += "* {{Vert|'''+'''}} ajout du bandeau sur [[%s]]\n" %(add)
        nbAdd += 1

    for rem in remList:
        log += "* {{Rouge|'''-'''}} retrait du bandeau sur [[%s]]\n" %(rem)
        nbRem += 1

    saveBackupFile(actualList)

    summary = "Mise à jour (+%s; -%s; =%s)" %(nbAdd, nbRem, total)

    return log, summary, total

#Return the content of the given category (only pages from namespace 0)
def getCategoryContent(catname: str) -> Iterator['pywikibot.page.Page']:
    cat = pywikibot.Category(site, catname)
    pagesInCat = list(cat.articles(recurse=False))
    pagesList = pagegenerators.PreloadingGenerator(pagesInCat) # On génère la liste des pages incluses dans la catégorie
    pagesList = pagegenerators.NamespaceFilterPageGenerator(pagesList, [0]) #On ne garde que les articles (Namespace 0)

    return pagesList

#Return the list of titles from pagesList
def titleList(pagesList: Iterator['pywikibot.page.Page']) -> List[str]:
    tlist = []
    for page in pagesList:
        tlist.append(page.title())

    return tlist

#Save a list to the backup file
def saveBackupFile(tlist: List[str]):
    bfile = open('_admissibilite.bak', mode='w+', encoding='UTF-8')
    for s in tlist:
        bfile.write(s + '\n')
    bfile.close()

#Load a list from the backup file
def loadBackupFile():
    bfile = open('_admissibilite.bak', mode='r', encoding='UTF-8')
    oldList = bfile.readlines()
    oldList = [s.strip('\n') for s in oldList]
    oldList = [s for s in oldList]
    bfile.close()

    return oldList


#Exécution
def main():
    log = ''
    timeStart = time.time()
    shutil.copyfile('_admissibilite.bak', '_admissibilite.bak.bak')
    catname = "Tous les articles dont l'admissibilité est à vérifier"
    pagesList = getCategoryContent(catname)
    log, summary, total = admissibilite(pagesList)
    grapher.update(total)
    logger.editLog(site, log, page="Projet:Maintenance/Suivi d'admissibilité", summary=summary, ar=False, cl=15)
    pywikibot.output(summary)
    timeEnd = time.time()
    pywikibot.output("%s in %s s."
                        %(summary, round(timeEnd-timeStart, 2)))


if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

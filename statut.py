#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de mise à jour de statut

# Auteur: Orikrin

# Adapté depuis un script de Linedwell
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import pywikibot
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
site = pywikibot.Site('fr', 'wikipedia')
statuts = ['online', 'offline', 'away', 'busy', 'patrol', 'redact', 'wikislow', 'invisible']
title = 'Utilisateur:Orikrin1998/Statut'


#Exécution
def main(args):
    if len(args) > 1: #argv[0] = nom du script
        stat = args[1]
        if stat in statuts:
            page = pywikibot.Page(site, title)
            pageTemp = "{{statut|" + stat + "|" + title + "}}"
            summary = "Mise à jour : " + stat

            if page.get() == pageTemp:
                pywikibot.output("Aucune mise à jour nécessaire.")
            else:
                page.text = pageTemp
                try:
                    page.save(summary)
                    pywikibot.output("Action effectuée.")
                except pywikibot.exceptions.PageSaveRelatedError as myexception:
                    pywikibot.output('%s %s'% (type(myexception), myexception.args))
        else:
            pywikibot.output("Statut '%s' inexistant\nStatuts disponibles : %s" %(stat, str(statuts)))
    else:
        pywikibot.output("usage: python " + args[0] + " <statut>")
        sys.exit(2)


if __name__ == "__main__":
    try:
        main(sys.argv)
    finally:
        pywikibot.stopme()

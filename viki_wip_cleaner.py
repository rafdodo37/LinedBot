#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de maintenance pour vikidia


# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import re
from datetime import datetime, timedelta

import pywikibot
from pywikibot import pagegenerators

import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import


# Déclarations
site = pywikibot.Site('fr', 'vikidia')

# Traitement des nouvelles pages
def cleanWIP(delay: int) -> str:
    limit = calcLimit(delay)
    log = ''

    wipCat = pywikibot.Category(site, "Article en travaux")
    wipPages = list(wipCat.articles(False))
    pagesList = pagegenerators.PreloadingGenerator(wipPages) # On génère la liste des pages incluses dans la catégorie
    pagesList = pagegenerators.NamespaceFilterPageGenerator(pagesList, [0]) #On ne garde que les articles (Namespace 0)


    for page in pagesList:

        try:
            pageTemp = page.get()

        except pywikibot.exceptions.NoPageError:
            pywikibot.output("Page %s does not exist; skipping."
                             % page.title(as_link=True))
        except pywikibot.exceptions.IsRedirectPageError:
            pywikibot.output("Page %s is a redirect; skipping."
                             % page.title(as_link=True))
        except pywikibot.exceptions.LockedPageError:
            pywikibot.output("Page %s is locked; skipping."
                             % page.title(as_link=True))
        else:
            summary = ''
            lastEdit = page.latest_revision.timestamp

            if lastEdit < limit:
                duration = calcDuration(lastEdit)
                pageTemp, templateResult = removeTemplate(pageTemp)
                summary = "[[VD:Robot|Robot]] : Retrait du bandeau %s (article non modifié depuis plus de %s jours)" %(templateResult, duration.days)
                pywikibot.showDiff(page.text, pageTemp)
                page.text = pageTemp
                pywikibot.output(summary)
                try:
                    page.save(summary)
                except pywikibot.exceptions.PageSaveRelatedError as myexception:
                    pywikibot.output('%s %s'% (type(myexception), myexception.args))

    return log

#Retourne la date avant laquelle on considère obsolète l'usage du modèle
def calcLimit(delay: int) -> 'datetime.datetime':
    today = datetime.utcnow()
    limite = today - timedelta(seconds=delay)
    return limite

#Retourne le temps écoulé entre une date et le jour courant
def calcDuration(date: 'datetime.datetime') -> 'datetime.timedelta':
    today = datetime.utcnow()
    duration = today - date
    return duration

# Retrait du bandeau si besoin
def removeTemplate(pageTemp: str) -> Tuple[str, str]:
    templateResult = ''
    parser = re.compile(r'{{(En)?Trav(ail|aux)(?:\|.*?)?}}(?P<fin>\r\n|\n|\ )?', re.I | re.U | re.DOTALL)
    searchResult = parser.search(pageTemp)
    if searchResult:
        pageTemp = parser.sub('', pageTemp, 1)
        templateResult = searchResult.group()
        templateResult = templateResult.replace('\r\n', '') #Retire les sauts de ligne contenus dans le modèle avant de l'ajouter au résumé
        templateResult = templateResult.replace('\n', '') #Correspond au second type de retour à la ligne
    return pageTemp, templateResult

# Exécution
def main():
    cleanWIP(delay=15552000)

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

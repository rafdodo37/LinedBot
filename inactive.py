#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script relevant les sysops absents depuis un temps donné

# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html


import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import
import utils

from datetime import datetime, timedelta

import sqlite3

import pywikibot

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Déclarations
dico = ''
currentYear = datetime.utcnow().year
currentMonth = datetime.utcnow().month

dicoCA = {
    'site' : pywikibot.Site('ca', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}

dicoDE = {
    'site' : pywikibot.Site('de', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}


dicoEL = {
    'site' : pywikibot.Site('el', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}


dicoEN = {
    'site' : pywikibot.Site('en', 'vikidia'),
    'page' : "Vikidia:Requests/Bureaucrats",
    'month' : ['', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
    'days' : "days",
    'since' : " inactive since ",
    'template' : "\n\n{{User:LinedBot/InactiveAdminNotification|%s|%s}}\n",
    'section' : "\n\n== Administrators inactive for more than one year ==\n\n",
    'notifsummary' : "[[VD:Robot|Robot]] : Notification of incoming tools removal",
    'reportsummary' : "[[VD:Robot|Robot]] : List of inactive administrators from atleast one year",
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}

dicoES = {
    'site' : pywikibot.Site('es', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}

dicoEU = {
    'site' : pywikibot.Site('eu', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}


dicoFR = {
    'site' : pywikibot.Site('fr', 'vikidia'),
    'page' : "Vikidia:Demandes aux bureaucrates/%s %02d" % (str(currentYear), currentMonth),
    'month' : ['', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
    'days' : "jours",
    'since' : " inactif depuis le ",
    'template' : "\n\n{{subst:Utilisateur:LinedBot/NotifAdminInactif|%s|%s}}\n",
    'section' : "\n\n== Administrateurs inactifs depuis au moins un an ==\n\n",
    'notifsummary' : "[[VD:Robot|Robot]] : Notification de prochaine suspension des outils",
    'reportsummary' : "[[VD:Robot|Robot]] : Liste des administrateurs inactifs depuis au moins un an",
    'hard' : 365,
    'soft' : 300,
    'simulation' : False,
}

dicoHY = {
    'site' : pywikibot.Site('hy', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}



itmonth = ['', 'gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre']

dicoIT = {
    'site' : pywikibot.Site('it', 'vikidia'),
    'page' : "Vikidia:Richieste agli amministratori/%s %s" % (str(itmonth[currentMonth]), str(currentYear)),
    'month' : ['', 'gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre'],
    'days' : "giorni",
    'since' : " inattivo dal ",
    'template' : "\n\n{{subst:Utente:LinedBot/NotificaAdminInattivo|%s|%s}}\n",
    'section' : "\n\n== Amministratori inattivi da almeno un anno ==\n\n",
    'notifsummary' : "[[Vikidia:Bot|Bot]]: Notifica di sospensione prossima degli strumenti di amministratore",
    'reportsummary' : "[[Vikidia:Bot|Bot]]: Lista dei amministratori inattivi da più di un anno",
    'hard' : 365,
    'soft' : 300,
    'simulation' : False,
}

dicoPT = {
    'site' : pywikibot.Site('pt', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}

dicoRU = {
    'site' : pywikibot.Site('ru', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}


dicoSCN = {
    'site' : pywikibot.Site('scn', 'vikidia'),
    'hard' : 365,
    'soft' : 300,
    'simulation' : True,
}


#BUGFIX
#site.login()
#END OF FIX

#Retourne la liste des administrateurs ainsi que la date de leur dernière contribution
def getSysopsLastEdit() -> Dict[str, 'datetime.datetime']:
    site = dico['site']
    sysopList = site.allusers(group="sysop")
    sysopLastEdit = {}

    for sysop in sysopList:
        sysopName = sysop['name']
        uc = site.usercontribs(user=sysopName, total=1)

        for c in uc:
            lastEdit = datetime.strptime(c['timestamp'], "%Y-%m-%dT%H:%M:%SZ")
            sysopLastEdit[sysopName] = lastEdit
            pywikibot.output("%s : %s" %(sysopName, utils.calcDuration(lastEdit)))
            break
    return sysopLastEdit

#Retourne la liste des administrateurs inactifs depuis <hardlimit>, notifie ceux inactifs depuis <softlimit>
def getInactiveSysops(sysop_list: Dict[str, 'datetime.datetime'])-> Tuple[List[Tuple[str, 'datetime.datetime']], List[Tuple[str, 'datetime.datetime']]]:
    hardlimit = utils.calcLimit(dico['hard'])
    softlimit = utils.calcLimit(dico['soft'])
    inactiveSysopsHard = []
    inactiveSysopsSoft = []

    for sysop in sorted(sysop_list.keys()):
        lastEdit = sysop_list[sysop]
        if lastEdit < hardlimit:
            inactiveSysopsHard.append([sysop, lastEdit])
        elif lastEdit < softlimit:
            inactiveSysopsSoft.append([sysop, lastEdit])

    return inactiveSysopsHard, inactiveSysopsSoft

#Notifie la liste des admins ayant presque atteint le seuil d'inactivité de la possible suspension de leurs outils
def notifySysop(sysop_list: List[Tuple[str, 'datetime.datetime']]):
    if len(sysop_list) > 0:
        for i in sysop_list:
            sysop, lastEdit = i
            page = pywikibot.Page(dico['site'], "User talk:"+sysop)
            status = db_check_status(dico['site'].lang, sysop)
            if not status:
                duration = utils.calcDuration(lastEdit)
                hrdate = "%s %s %s (%s %s)" %(lastEdit.day, dico['month'][int(lastEdit.month)], lastEdit.year, duration.days, dico['days'])
                deadline = lastEdit + timedelta(days=dico['hard'])
                hrdeadline = "%s %s %s" %(deadline.day, dico['month'][int(deadline.month)], deadline.year)
                notif = dico['template'] %(hrdate, hrdeadline)

                summary = dico['notifsummary']
                page.text = page.text + notif
                try:
                    page.save(summary, minor=False)
                except pywikibot.exceptions.PageSaveRelatedError as myexception:
                    pywikibot.error('%s %s'% (type(myexception), myexception.args))
                db_upsert_status(dico['site'].lang, sysop, lastEdit, "N")

            else:
                pywikibot.output("%s already notified; skipping." % page.title(as_link=True))

#Envoie sur VD:DB la liste des administrateurs inactifs ainsi que la durée de leur inactivité
def reportInactiveSysops(sysop_list: List[Tuple[str, 'datetime.datetime']]):
    page = pywikibot.Page(dico['site'], dico['page'])

    if len(sysop_list) > 0:
        section = dico['section']
        report = ''

        for i in sysop_list:
            sysop, lastEdit = i
            status = db_check_status(dico['site'].lang, sysop)

            # Si l'administrateur n'a pas déjà été reporté pour cette absence
            if not status or not status[0] == "R":
                db_upsert_status(dico['site'].lang, sysop, lastEdit, "R")
                duration = utils.calcDuration(lastEdit)
                hrdate = "%s %s %s (%s %s)" %(lastEdit.day, dico['month'][int(lastEdit.month)], lastEdit.year, duration.days, dico['days'])
                report += "* {{u|" + sysop + "}} : " + dico['since'] + hrdate + "\n"
            else:
                pywikibot.output("%s already reported; skipping." % sysop)

        if len(report):
            page.text = page.text + section + report
            summary = dico['reportsummary']
            try:
                page.save(summary, minor=False, botflag=False)
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.error('%s %s'% (type(myexception), myexception.args))
        else:
            pywikibot.output("Nobody to report.")


def cleanup_obsolete_entries():
    all_inactive = db_get_all_inactive()
    for i in all_inactive:
        project, username, _, _ = i
        site = pywikibot.Site(project, 'vikidia')
        user = pywikibot.User(site, username)
        if (not user.isRegistered()) or ('sysop' not in user.groups()):
            pywikibot.output("%s deleted; not a sysop" %(username))
            db_delete_status(project, username)

# Vérifie si l'admin a déjà été notifié / signalé pour inactivité
def db_check_status(project: str, sysop: str):
    table = "inactive"
    conn = sqlite3.connect('db/inactive.db')
    cursor = conn.cursor()
    cursor.execute("""
    SELECT status FROM """ + table + """
    WHERE project = ? AND
    username = ?
    """, [project, sysop])
    result = cursor.fetchone()
    conn.close()
    return result

# Ajoute (INSERT) ou modifie (REPLACE) le statut du signalement d'un admin
def db_upsert_status(project: str, sysop: str, lastEdit: str, status: str):
    table = "inactive"
    conn = sqlite3.connect('db/inactive.db')
    cursor = conn.cursor()
    cursor.execute("""
    INSERT OR REPLACE INTO """ + table + """(project, username, lastedit, status)
    VALUES(?, ?, ?, ?)
    """, [project, sysop, str(lastEdit), status])
    conn.commit()
    conn.close()

def db_delete_status(project: str, sysop: str):
    table = "inactive"
    conn = sqlite3.connect('db/inactive.db')
    cursor = conn.cursor()
    cursor.execute("""
    DELETE FROM """ + table + """
    WHERE project = ? AND username = ?
    """, [project, sysop])
    conn.commit()
    conn.close()

def db_get_all_inactive():
    table = "inactive"
    conn = sqlite3.connect('db/inactive.db')
    cursor = conn.cursor()
    cursor.execute("""
    SELECT * from """ + table)
    result = cursor.fetchall()
    conn.close()
    return result

#Lanceur principal
def inactiveSysopsManager(loc: dict, simulation=False):
    global dico
    dico = loc
    dico['site'].login()
    sysopLastEdit = getSysopsLastEdit()
    inactiveSysopsHard, inactiveSysopsSoft = getInactiveSysops(sysopLastEdit)

    if not simulation:
        notifySysop(inactiveSysopsSoft)
        reportInactiveSysops(inactiveSysopsHard)

#Exécution
def main():
    for i in ["FR", "IT", "CA", "DE", "EL", "EN", "ES", "EU", "HY", "PT", "RU", "SCN"]:
        iwiki = eval("dico" + str(i)) # pylint: disable=eval-used
        pywikibot.output(i)
        inactiveSysopsManager(iwiki, iwiki['simulation'])
    cleanup_obsolete_entries()

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

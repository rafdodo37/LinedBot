#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script d'accueil pour vikidia


# (C) Linedwell, 2011-2020
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import re
from datetime import timedelta

from random import choice

import pywikibot
from pywikibot import i18n
from pywikibot.tools.formatter import color_format

import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import


# Déclarations

globalsettings = {
    'reqEditCount' : 1,         #number of edits required to welcome an user
    'timeoffset' : 60,          #skip users newer than # minutes
    'defaultSign' : '--~~~~',   #default signature
    'queryLimit' : 50,          #number of users loaded by the bot
}

welcomemsg = {
    'fr' : '{{subst:Bienvenue}}%s',
    'it' : '{{subst:Benvenuto}}%s',
}

welcomeschoolmsg = {
    'fr' : '{{subst:Bienvenue école}}%s',
}

schooldiscriminator = {
    'fr' : 'coll[eè]ge|[eé]cole|cdi|lyc[eé]|prof',
}

welcomelog = {
    'fr' : 'User:LinedBot/Welcome/Log',
    'it' : 'User:LinedBot/Welcome/Log',
}

random_sign = {
    #'fr' : 'User:LinedBot/Welcome/Signatures',
    'it' : 'User:LinedBot/Welcome/Firme',
    #'it' : 'wp:Wikipedia:Benvenuto Bot/Firme',
}

class WelcomeBot:

    # bot initialization
    def __init__(self, viki):
        self.site = pywikibot.Site(viki, 'vikidia')
        self.checkManagedSites()
        self.randomSign = self.getRandomSign()
        self.welcomed_users = list()

    # chek if "autowelcome" is enabled or not on the wiki
    def checkManagedSites(self):
        wlcmsg = i18n.translate(self.site, welcomemsg)
        if wlcmsg is None:
            raise KeyError(
                'welcome.py is not localized for site {0} in welcomemsg dict.'
                ''.format(self.site))

    # random list of signatures used for the welcome message
    def getRandomSign(self):
        signPageName = i18n.translate(self.site, random_sign)
        talkPageLinkText = pywikibot.Page(self.site, "MediaWiki:Talkpagelinktext").get()
        if not signPageName:
            pywikibot.output("Random signature disabled on %s; disabling random sign" % self.site)
            return None

        signPage = pywikibot.Page(self.site, signPageName)

        if signPage.exists():
            #sign = ''
            signList = list(signPage.linkedPages(namespaces=2))
            randomSignatory = choice(signList)
            randomSign = "--[[User:{u}|{u}]] ([[User_talk:{u}|{t}]]) ~~~~~".format(u=randomSignatory.title(with_ns=False), t=talkPageLinkText)
            return randomSign
        else:
            pywikibot.output("The random signature list doesn't exist; disabling random sign")
            return None

    # retrieve new users list
    def parseNewUserLog(self):
        start = self.site.server_time() - timedelta(minutes=globalsettings['timeoffset'])
        for ue in self.site.logevents('newusers', total=globalsettings['queryLimit'], start=start):
            try:
                yield pywikibot.User(ue.page())
            except pywikibot.exceptions.HiddenKeyError:
                pywikibot.output("Hidden entry; skipping")
                continue


    def run(self):
        for user in self.parseNewUserLog():
            #print user.title()
            if user.isBlocked():
                #showStatus(3)
                pywikibot.output("%s is blocked; skipping." % user.title())
                continue
            if "bot" in user.groups():
                #showStatus(3)
                pywikibot.output("%s is a bot; skipping." % user.title())
                continue
            if "bot" in user.title().lower():
                #showStatus(3)
                pywikibot.output("%s is probably a bot; skipping." % user.title())
                continue
            if user.editCount() >= globalsettings['reqEditCount']:
                #showStatus(2)
                pywikibot.output("%s has enough edits to be welcomed." % user.title())
                ustp =  user.getUserTalkPage()
                if ustp.exists():
                    #showStatus(3)
                    pywikibot.output("%s has been already welcomed; skipping." % user.title())
                    continue
                else:
                    welcome_text = i18n.translate(self.site, welcomemsg)
                    welcome_school_text = i18n.translate(self.site, welcomeschoolmsg)
                    if welcome_school_text != None:
                        discrim = i18n.translate(self.site, schooldiscriminator)
                        if discrim != None and re.match(discrim, user.title().lower()): #if the discriminator exists and is present in username
                            pywikibot.output("%s is a maybe a school related account; using school welcoming template instead." % user.title())
                            welcome_text = welcome_school_text
                    sign = self.randomSign if self.randomSign is not None else globalsettings['defaultSign']

                    welcome_text = (welcome_text % sign)
                    welcome_cmnt = "Robot: " + i18n.twtranslate(self.site, 'welcome-welcome')

                    ustp.text = welcome_text

                    try:
                        ustp.save(welcome_cmnt, minor=False)
                    except pywikibot.exceptions.EditConflictError:
                        pywikibot.output("An edit conflict has occurred, "
                                             "skipping %s." % user.title())
                    except pywikibot.exceptions.PageSaveRelatedError as myexception:
                        pywikibot.output('%s %s'% (type(myexception), myexception.args))


def showStatus(n=0):
    """Output colorized status."""
    staColor = {
        0: 'lightpurple',
        1: 'lightaqua',
        2: 'lightgreen',
        3: 'lightyellow',
        4: 'lightred',
        5: 'lightblue'
    }
    staMsg = {
        0: 'MSG',
        1: 'NoAct',
        2: 'Match',
        3: 'Skip',
        4: 'Warning',
        5: 'Done',
    }
    pywikibot.output(color_format('{color}[{0:5}]{default} ',
                                  staMsg[n], color=staColor[n]), newline=False)

# Exécution
def main():

    site = sys.argv[1]
    try:
        wb = WelcomeBot(site)
    except KeyError as error:
        # site not managed by welcome.py
        pywikibot.error(error)
        return False
    wb.run()

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
